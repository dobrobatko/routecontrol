package com.example.router.services;

import com.example.router.characteristics.AirplaneCharacteristics;
import com.example.router.flight.TemporaryPoint;
import com.example.router.flight.WayPoint;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class PlaneCalculation {

    final int KM_IN_DEGREE_LATITUDE = 70;
    final int KM_IN_DEGREE_LONGITUDE = 111;

    public PlaneCalculation() {}

    public List<TemporaryPoint> calculateRoute(AirplaneCharacteristics characteristics, List<WayPoint> wayPoints) {
        List<TemporaryPoint> results = new ArrayList<>();

        ArrayList<WayPoint> wayPointsArray = (ArrayList<WayPoint>) wayPoints;
        wayPointsArray.add(wayPointsArray.size(), null);

        for (int i = 0; (i + 1) < wayPointsArray.size(); i++) { // Ітерація вказаних контрольних points
            if (wayPointsArray.get(i + 1) != null) {            // Перевіряємо, чи є наступна

                double currentLatitude = wayPointsArray.get(i).getLatitude();
                double currentLongitude = wayPointsArray.get(i).getLongitude();
                int currentAltitude = wayPointsArray.get(i).getAltitude();
                int currentSpeed = wayPointsArray.get(i).getSpeed();
                double currentAzimuth;

                //Розраховуємо відстань між точками за за теоремою Піфагора (в метрах)
                double projectionY = Math.abs(wayPointsArray.get(i).getLongitude() - wayPointsArray.get(i + 1).getLongitude()) * KM_IN_DEGREE_LONGITUDE * 1000;
                double projectionX = Math.abs(wayPointsArray.get(i).getLatitude() - wayPointsArray.get(i + 1).getLatitude()) * KM_IN_DEGREE_LATITUDE * 1000;
                int distance = (int) Math.sqrt(Math.pow(projectionY, 2) + Math.pow(projectionX, 2));

                //Визначаємо напрям руху
                int quarter = 0;
                double azimuth = 360;
                if (wayPointsArray.get(i).getLongitude() < wayPointsArray.get(i + 1).getLongitude() &&
                        wayPointsArray.get(i).getLatitude() < wayPointsArray.get(i + 1).getLatitude()) quarter = 1;
                if (wayPointsArray.get(i).getLongitude() > wayPointsArray.get(i + 1).getLongitude() &&
                        wayPointsArray.get(i).getLatitude() < wayPointsArray.get(i + 1).getLatitude()) quarter = 2;
                if (wayPointsArray.get(i).getLongitude() > wayPointsArray.get(i + 1).getLongitude() &&
                        wayPointsArray.get(i).getLatitude() > wayPointsArray.get(i + 1).getLatitude()) quarter = 3;
                if (wayPointsArray.get(i).getLongitude() < wayPointsArray.get(i + 1).getLongitude() &&
                        wayPointsArray.get(i).getLatitude() > wayPointsArray.get(i + 1).getLatitude()) quarter = 4;
                if (wayPointsArray.get(i).getLongitude() < wayPointsArray.get(i + 1).getLongitude() &&
                        wayPointsArray.get(i).getLatitude() == wayPointsArray.get(i + 1).getLatitude()) azimuth = 0;
                if (wayPointsArray.get(i).getLongitude() == wayPointsArray.get(i + 1).getLongitude() &&
                        wayPointsArray.get(i).getLatitude() < wayPointsArray.get(i + 1).getLatitude()) azimuth = 90;
                if (wayPointsArray.get(i).getLongitude() > wayPointsArray.get(i + 1).getLongitude() &&
                        wayPointsArray.get(i).getLatitude() == wayPointsArray.get(i + 1).getLatitude()) azimuth = 180;
                if (wayPointsArray.get(i).getLongitude() == wayPointsArray.get(i + 1).getLongitude() &&
                        wayPointsArray.get(i).getLatitude() > wayPointsArray.get(i + 1).getLatitude()) azimuth = 270;

                //Знаходимо курс
                double sinAlpha;
                double radAlpha;
                switch (quarter) {
                    case 1:
                        sinAlpha = projectionX / distance;
                        radAlpha = Math.asin(sinAlpha);
                        azimuth = Math.toDegrees(radAlpha);
                        break;
                    case 2:
                        sinAlpha = projectionY / distance;
                        radAlpha = Math.asin(sinAlpha);
                        azimuth = Math.toDegrees(radAlpha) + 90;
                        break;
                    case 3:
                        sinAlpha = projectionX / distance;
                        radAlpha = Math.asin(sinAlpha);
                        azimuth = Math.toDegrees(radAlpha) + 180;
                        break;
                    case 4:
                        sinAlpha = projectionY / distance;
                        radAlpha = Math.asin(sinAlpha);
                        azimuth = Math.toDegrees(radAlpha) + 270;
                        break;
                }
                currentAzimuth = azimuth;

                // В залежності від напрямку руху, вказуємо, як будуть розраховуватися короткотермінові проекції на довготу та широту
                // Знаходимо проекції в метрах, переводимо в градуси
                double alphaRad = 0;
                double alphaSin = 0;
                double alphaCos = 0;
                int deltaRoute = 0;
                double distanceY = 0;
                double distanceX = 0;
                double deltaLongitude = 0;
                double deltaLatitude = 0;

                deltaRoute = currentSpeed; // у випадку, якщо точки маршруту будуються з інтервалом 1 сек.

                switch (quarter) {
                    case 1:
                        alphaRad = Math.toRadians(currentAzimuth);
                        alphaSin = Math.sin(alphaRad);
                        alphaCos = Math.cos(alphaRad);
                        distanceY = alphaCos * deltaRoute;
                        distanceX = alphaSin * deltaRoute;
                        deltaLongitude = distanceY / (KM_IN_DEGREE_LONGITUDE * 1000);
                        deltaLatitude = distanceX / (KM_IN_DEGREE_LATITUDE * 1000);
                        break;
                    case 2:
                        alphaRad = Math.toRadians(currentAzimuth - 90);
                        alphaSin = Math.sin(alphaRad);
                        alphaCos = Math.cos(alphaRad);
                        distanceY = alphaSin * deltaRoute;
                        distanceX = alphaCos * deltaRoute;
                        deltaLongitude = -distanceY / (KM_IN_DEGREE_LONGITUDE * 1000);
                        deltaLatitude = distanceX / (KM_IN_DEGREE_LATITUDE * 1000);
                        break;
                    case 3:
                        alphaRad = Math.toRadians(currentAzimuth - 180);
                        alphaSin = Math.sin(alphaRad);
                        alphaCos = Math.cos(alphaRad);
                        distanceY = alphaCos * deltaRoute;
                        distanceX = alphaSin * deltaRoute;
                        deltaLongitude = -distanceY / (KM_IN_DEGREE_LONGITUDE * 1000);
                        deltaLatitude = -distanceX / (KM_IN_DEGREE_LATITUDE * 1000);
                        break;
                    case 4:
                        alphaRad = Math.toRadians(currentAzimuth - 270);
                        alphaSin = Math.sin(alphaRad);
                        alphaCos = Math.cos(alphaRad);
                        distanceY = alphaSin * deltaRoute;
                        distanceX = alphaCos * deltaRoute;
                        deltaLongitude = distanceY / (KM_IN_DEGREE_LONGITUDE * 1000);
                        deltaLatitude = -distanceX / (KM_IN_DEGREE_LATITUDE * 1000);
                        break;
                }

                // OR

                switch ((int) azimuth) {
                    case 0:
                        deltaLatitude = 0;
                        deltaLongitude = deltaRoute / (KM_IN_DEGREE_LONGITUDE * 1000);
                        break;
                    case 90:
                        deltaLongitude = 0;
                        deltaLatitude = deltaRoute / (KM_IN_DEGREE_LATITUDE * 1000);
                    case 180:
                        deltaLatitude = 0;
                        deltaLongitude = -(deltaRoute / (KM_IN_DEGREE_LONGITUDE * 1000));
                    case 270:
                        deltaLongitude = 0;
                        deltaLatitude = -(deltaRoute / (KM_IN_DEGREE_LATITUDE * 1000));

                }

                int time = distance / currentSpeed;
                // Корегуємо широту і довготу в залежності від пройденого шляху, додаємо позицію
                for (int j = 1; j <= time; j++) {
                    currentLatitude += deltaLatitude;
                    currentLongitude += deltaLongitude;
                    TemporaryPoint tp = new TemporaryPoint(currentLatitude, currentLongitude, currentAltitude, currentSpeed, currentAzimuth);
                    results.add(tp);
                }
            }
        }
        return results;
    }
}
