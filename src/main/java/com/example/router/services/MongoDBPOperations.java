package com.example.router.services;

import com.example.router.models.Airplane;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

@Service
public class MongoDBPOperations {

    public void saveAirplane(MongoOperations mongoOperations, Airplane airplane) {
        mongoOperations.save(airplane);
        System.out.println("Saved -> " + airplane);
    }
}
