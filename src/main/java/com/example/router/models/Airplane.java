package com.example.router.models;

import com.example.router.characteristics.AirplaneCharacteristics;
import com.example.router.configs.MongoConfig;
import com.example.router.flight.Flight;
import com.example.router.flight.TemporaryPoint;
import com.example.router.flight.WayPoint;
import com.example.router.services.MongoDBPOperations;
import com.example.router.services.PlaneCalculation;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "airplanes")
public class Airplane {
    @Id
    private String id;
    AirplaneCharacteristics characteristics;
    TemporaryPoint position;
    List<Flight> flights = new ArrayList<>();

    public Airplane(AirplaneCharacteristics characteristics) {
        this.characteristics = characteristics;

    }

    public boolean goFly (List<WayPoint> wayPoints) {
        System.out.println("This airplane has completed " + flights.size() + " flights.");
        if(flights.size() !=0) {
            for(Flight flight : flights) {
                System.out.println(flight.toString());
            }
        }
        try{
            ApplicationContext ctx = new AnnotationConfigApplicationContext(MongoConfig.class);
            MongoOperations mongoOperations = (MongoOperations) ctx.getBean("mongoTemplate");
            MongoDBPOperations mongoDBPOperations = new MongoDBPOperations();

            PlaneCalculation planeCalculation = new PlaneCalculation();

            List<TemporaryPoint> passedPoints = planeCalculation.calculateRoute(characteristics, wayPoints);

            Flight flight = new Flight(flights.size()+1, wayPoints, passedPoints);
            flights.add(flight);

            mongoDBPOperations.saveAirplane(mongoOperations, this);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AirplaneCharacteristics getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(AirplaneCharacteristics characteristics) {
        this.characteristics = characteristics;
    }

    public TemporaryPoint getPosition() {
        return position;
    }

    public void setPosition(TemporaryPoint position) {
        this.position = position;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }
}
