package com.example.router.characteristics;

public class AirplaneCharacteristics {
    int maxSpeed;
    int maxAcceleration;
    int heightChangeSpeed;
    int courseChangeSpeed;

    public AirplaneCharacteristics(int maxSpeed, int maxAcceleration, int heightChangeSpeed, int courseChangeSpeed) {
        this.maxSpeed = maxSpeed;
        this.maxAcceleration = maxAcceleration;
        this.heightChangeSpeed = heightChangeSpeed;
        this.courseChangeSpeed = courseChangeSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getMaxAcceleration() {
        return maxAcceleration;
    }

    public void setMaxAcceleration(int maxAcceleration) {
        this.maxAcceleration = maxAcceleration;
    }

    public int getHeightChangeSpeed() {
        return heightChangeSpeed;
    }

    public void setHeightChangeSpeed(int heightChangeSpeed) {
        this.heightChangeSpeed = heightChangeSpeed;
    }

    public int getCourseChangeSpeed() {
        return courseChangeSpeed;
    }

    public void setCourseChangeSpeed(int courseChangeSpeed) {
        this.courseChangeSpeed = courseChangeSpeed;
    }
}
