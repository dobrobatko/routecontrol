package com.example.router.flight;

import com.example.router.services.PlaneCalculation;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class Flight {
    public int number;
    public List<WayPoint> controlPoints;
    public List<TemporaryPoint> passedPoints;
    Long time;

    public Flight(int number, List<WayPoint> controlPoints, List<TemporaryPoint> passedPoints) {
        this.number = number;
        this.controlPoints = controlPoints;
        this.passedPoints = passedPoints;
        this.time = Long.valueOf(controlPoints.size() + passedPoints.size());
    }


    public int getNumber() {
        return number;
    }

    public List<WayPoint> getControlPoints() {
        return controlPoints;
    }

    public List<TemporaryPoint> getPassedPoints() {
        return passedPoints;
    }

    public Long getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "number=" + number + " " +
                ", time=" + time + " seconds" +
                '}';
    }
}
