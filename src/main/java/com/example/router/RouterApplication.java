package com.example.router;

import com.example.router.characteristics.AirplaneCharacteristics;
import com.example.router.flight.WayPoint;
import com.example.router.models.Airplane;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class RouterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RouterApplication.class, args);
    }

    @Bean
    public CommandLineRunner start() {

        return strings -> {

            AirplaneCharacteristics characteristics = new AirplaneCharacteristics(20, 1, 1, 1);
            Airplane airplane = new Airplane(characteristics);

            List< WayPoint> wayPoints = new ArrayList<>();

            // Перший запуск
            wayPoints.add(new WayPoint(10, 30, 100, 10));
            wayPoints.add(new WayPoint(11, 29, 120, 10));

            if(airplane.goFly(wayPoints)) {
                System.out.println("Fly completed!");
            } else {
                System.out.println("Something went wrong.");
            }

            // Другий запуск
            wayPoints.clear();
            wayPoints.add(new WayPoint(11, 29, 120, 10));
            wayPoints.add(new WayPoint(12, 30, 100, 10));

            if(airplane.goFly(wayPoints)) {
                System.out.println("Fly completed!");
            } else {
                System.out.println("Something went wrong.");
            }
        };
    }
}
